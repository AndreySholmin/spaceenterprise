package com.sholmin.spaceenterprise.dao;

import com.sholmin.spaceenterprise.entity.User;

import java.util.List;

public interface UserRepository {
    User saveUser(User user);

    User updateUser(User user);

    String updateUsername(String username, int id);

    User getById(int id);

    String deleteById(int id);

    List<User> allUsers();
}
