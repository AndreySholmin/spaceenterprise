package com.sholmin.spaceenterprise.dao;

import com.sholmin.spaceenterprise.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final String INSERT_USER_QUERY = "INSERT INTO USERS (id,username,email) values(?,?,?)";
    private static final String UPDATE_USER_BY_ID_QUERY = "UPDATE USERS SET username=?, email=? WHERE ID=?";
    private static final String GET_USER_BY_ID_QUERY = "SELECT * FROM USERS WHERE ID=?";
    private static final String DELETE_USER_BY_ID = "DELETE FROM USERS WHERE ID=?";
    private static final String GET_USERS_QUERY = "SELECT * FROM USERS";
    private static final String UPDATE_USERNAME_BY_ID_QUERY = "UPDATE USERS SET username=? WHERE ID=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public User saveUser(User user) {
        jdbcTemplate.update(INSERT_USER_QUERY, user.getId(), user.getUsername(), user.getEmail());
        return user;
    }

    @Override
    public User updateUser(User user) {
        jdbcTemplate.update(UPDATE_USER_BY_ID_QUERY, user.getUsername(),  user.getEmail(), user.getId());
        return user;
    }

    @Override
    public String updateUsername(String username, int id) {
        jdbcTemplate.update(UPDATE_USERNAME_BY_ID_QUERY, username, id);
        return "username update";
    }

    @Override
    public User getById(int id) {
        return jdbcTemplate.queryForObject(GET_USER_BY_ID_QUERY, (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("email")),id);
    }

    @Override
    public String deleteById(int id) {
        jdbcTemplate.update(DELETE_USER_BY_ID, id);
        return "User got deleted with id " + id;
    }

    @Override
    public List<User> allUsers() {
        return jdbcTemplate.query(GET_USERS_QUERY, (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("email")));
    }
}
