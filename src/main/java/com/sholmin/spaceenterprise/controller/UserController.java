package com.sholmin.spaceenterprise.controller;

import com.sholmin.spaceenterprise.dao.UserRepository;
import com.sholmin.spaceenterprise.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @PostMapping()
    public User addUser(@RequestBody User user) {
        return userRepository.saveUser(user);
    }

    @PutMapping()
    public User updateUser(@RequestBody User user) {
        return userRepository.updateUser(user);
    }

    @PatchMapping("/{id}")
    public String updateUsername(@RequestParam String username, @PathVariable(value ="id") int id) {
        return userRepository.updateUsername(username, id);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int id) {
        return userRepository.getById(id);
    }

    @GetMapping()
    public List<User> getUsers() {
        return userRepository.allUsers();
    }

    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable("id") int id){
        return userRepository.deleteById(id);
    }

}
